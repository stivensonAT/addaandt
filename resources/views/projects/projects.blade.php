@extends('layouts.app')
@section('content')

<!-- page content -->
<div class="right_col" role="main">

  <!-- Table content -->
  <div class="">
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Listado <small>Usuarios</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              Use el buscador para facilitar la consulta del usuario,  si da click encima del nombre o documento podrá editar y ver mas información del usuario, tambien puede descargar hacer unso de la información en formato PDF, EXCEL Y CSV
            </p>
            <table id="datatable-buttons" class="table table-striped table-bordered">
              <thead>
                <tr>

                  <th>Nombre</th>
                  <th>codigo</th>
                  <th>Aceleradores</th>
                  <th>vistas</th>
                  <th>precio</th>

                </tr>
              </thead>
              <tbody>
                @foreach ($tableprojects as $projectTable)

                <tr>
                  <td><a href="{{route('project_detal_paht',$projectTable->id)}}">{{$projectTable->name}}</a></td>
                  <td>{{$projectTable->project_code}}</td>
                  <td>{{count($projectTable->follow)}}</td>
                  <td>{{  number_format($projectTable->views+($projectTable->views * 20.0)/100)}}</td>
                  <td>{{$projectTable->price}}</td>

                </tr>

                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
@endsection
