<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{url('/')}}" class="site_title"><span>A&T Ecommerce</span></a>
        </div>
        <div class="clearfix"></div>
        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{asset('images/logoat.png')}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Bienvenido</span>
                <h2>{{ Auth::user()->name }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
            
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-users"></i>Usuarios</a></li>
                    <li>
                        <a><i class="fa fa-bar-chart-o"></i>Marketing<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="#">Usuarios</a></li>
                            <li><a href="#">Planes de usuario</a></li>
                            <li><a href="#">Estudio Tesos</a></li>
                            <li><a href="#">Agregar Nuevo</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-wrench"></i>Control<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="#">Usuarios</a></li>
                            <li><a href="#">Planes de usuario</a></li>
                            <li><a href="#">Estudio Tesos</a></li>
                            <li><a href="#">Agregar Nuevo</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

    </div>
</div>
