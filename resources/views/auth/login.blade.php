<!DOCTYPE html>
<html lang="en">
<head>
  @include('partials.newsheader')
</head>
<body>
   <div id="app">
    
        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{csrf_field()}}
                        <h1>Iniciar Sesión</h1>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email"  name="email"  class="form-control" placeholder="Email" required="" />
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input  id="password" name="password" type="password" class="form-control" placeholder="Password" required="" />
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div>
                            <button type="submit" class="btn btn-default submit">
                                Ingresar
                            </button>
                            <a class="reset_pass" href="{{ route('password.request') }}">  Olvide la Contraseña</a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link">  <a href="{{ route('register') }}" class="to_register"> Registrarme </a>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuerdame
                            </p>
                            <br />

                            <div>
                                <h1>A&T E-COMMERCE</h1>
                                <p>©2018 All Rights Reserved. A&T E-COMMERCE is a accounting software. Privacy and Terms</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</body>
</html>

