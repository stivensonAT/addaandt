<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.newsheader')

</head>
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
          <nav class="" role="navigation">
            <ul class="nav navbar-nav navbar-right">
              @if (!Auth::guest())
              <li>
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="images/logoat.png" alt=""> {{ Auth::user()->name }}
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <li><a href="javascript:;"> Mi Perfil</a></li>
                  <li>
                    <a href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Cerra Sesion
                  </a>
                  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </li>
              </ul>
            </li>
            @endif
          </ul>
        </nav>
      </div>
    </div>
    @include('partials.menu')  
    <!-- /page content -->
    @yield('content')
  </div>
</div>
@include('partials.newscrips')
</body>
</html>
