@extends('layouts.app')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Lading <small>Registros</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              Use el buscador para facilitar la consulta del usuario,  si da click encima del nombre o documento podrá editar y ver mas información del usuario, tambien puede descargar hacer unso de la información en formato PDF, EXCEL Y CSV
            </p>
            <table id="datatable-buttons" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Email</th>
                  <th>Celular/telefono</th>
                  <th>Lading</th>
                  <th>Estado</th>
                  <th>Registro</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tableUsers as $userTable)
                <tr>
                  <td>{{$userTable->name}}</td>
                  <td>{{$userTable->email}}</td>
                  <td>{{$userTable->phone}}</td>
                  <td>{{$userTable->landing}}</td>
                  @if($userTable->newslatter ==1)
                  <td>Acepto</td>
                  @elseif($userTable->newslatter ==2)
                  <td>Completo</td>
                  @endif
                  <td>{{date_format($userTable->created_at,'d-M-y ')}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
@endsection
