@extends('layouts.app')
@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Control</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Calidad </h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />

              <div class="form-group">
                <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-2">
                  <a href="{{route('mail_aviso_paht')}}"><button type="button" class="btn btn-primary">Enviar Aviso </button></a>

                </div>
              </div>

            <hr>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Table content -->
  <div class="">
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Listado <small>Usuarios</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              Use el buscador para facilitar la consulta del usuario,  si da click encima del nombre o documento podrá editar y ver mas información del usuario, tambien puede descargar hacer unso de la información en formato PDF, EXCEL Y CSV
            </p>
            <table id="datatable-buttons" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Nombre de usuario</th>
                  <th>email</th>
                  <th>tipo plan</th>
                  <th>Activo</th>
                  <th>Codigo U</th>
                  <th>fecha inicio</th>
                  <th>Fecha fin</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tableUsers as $userTable)
                @if($userTable->fecha_fin_subscripcion  <=  date('Y-m-d') )
                <tr>
                  <td><a href="{{route('detalles_usuario_email_phat', $userTable->email)}}">{{$userTable->username}}</a></td>
                  <td>{{$userTable->email}} </td>
                  <td><a href="{{route('plan_user_phat',$userTable->id)}}">
                    @if($userTable->target==1)
                    Natural
                    @elseif($userTable->target==2)
                    Inmobiliario
                    @elseif($userTable->target==3)
                    Constructora
                    @elseif($userTable->target==4)
                    Teso
                    @endif
                  </a></td>
                  <td>
                    <a href="{{route('plan_user_phat',$userTable->id)}}">
                      @if($userTable->activo==0)
                      <i class="fa fa-thumbs-down fa-2x" aria-hidden="true"></i>(no)
                      @elseif($userTable->activo==1)
                      <i class="fa fa-hand-o-left fa-2x" aria-hidden="true"></i>(si)
                      @elseif($userTable->activo==2)
                      <i class="  fa fa-hand-scissors-o fa-2x" aria-hidden="true"></i>(pago)

                      @endif
                    </a>
                  </td>
                  <td>{{$userTable->signature}}</td>
                  <td>{{$userTable->fecha_subscripcion}} </td>
                  <td>{{$userTable->fecha_fin_subscripcion}}</td>
                </tr>
                @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
@endsection
