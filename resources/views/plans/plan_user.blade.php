

@extends('layouts.app')

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Plan {{$planActual->signature}}</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Activa Plan<small>Detalles del plan comprado</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form method="POST" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('editar_plan_usuario',$planActual->id) }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Codigo Pay_U<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$planActual->signature}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Email<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$user->email}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Contacto<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$user->phone}} - {{$user->cell_phone}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Usuario<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$user->first_name}}, -{{$user->business_name}}, {{$user->username}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Target<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  @if($detalle->target==1)
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="Natural">
                  @elseif($detalle->target==2)
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="Inmobiliario">
                  @elseif($detalle->target==3)
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="Constructora">
                  @elseif($detalle->target==4)
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="Teso">
                  @endif
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha inicio<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$planActual->fecha_subscripcion}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha fin<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  name="fecha_fin" class="form-control col-md-7 col-xs-12" value="{{$planActual->fecha_fin_subscripcion}}">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Estado</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  @if($planActual->activo ==0)
                  <input type="hidden" name="hidactivo" value="0">
                  <div id="gender" class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="activo" value="1"> &nbsp;
                      <i class="fa fa-hand-o-left fa-2x" aria-hidden="true"></i> &nbsp;
                    </label>
                    <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">

                      <input type="radio" name="activo" value="0">
                      <i class="fa fa-thumbs-down fa-2x" aria-hidden="true"></i>
                    </label>
                  </div>

                  @else
                  <input type="hidden" name="hidactivo" value="1">
                  <div id="gender" class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="activo" value="0"> &nbsp;
                      <i class="fa fa-thumbs-down fa-2x" aria-hidden="true"></i> &nbsp;
                    </label>
                    <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="activo" value="1">
                      <i class="fa fa-hand-o-left fa-2x" aria-hidden="true"></i>
                    </label>
                  </div>
                  @endif
                </div>
              </div>
              <br>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button type="submit" class="btn btn-success">Actualizar Estado</button>

                  <a href="{{route('planes_has_phat')}}"><button type="button" class="btn btn-primary">Atras</button></a>
                </div>
              </div>
            </form>
            <hr>
            <div class="col-md-12">
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Costo Plan
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$detalle->price}}">
                </div>
              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Nombre
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$detalle->name}}">
                </div>
              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Numero publicaciones
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$detalle->number_of_objects}}">
                </div>
              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Numero de dias
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$detalle->number_of_days_published}}">
                </div>
              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Numero de fotos
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$detalle->number_of_images}}">
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <hr><p>Proyectos</p>
              @foreach ($project as $prop)
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Proyecto
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$prop->name}}">
                </div>

              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                </label>
                @if($prop->status == 1)
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <form action="{{url('bloquear_property',$planActual->id)}}" method="post">
                    {{ csrf_field() }}
                    <input type="text"  disabled class="form-control col-md-6 col-xs-12" value="Activo">
                    <br>
                    <input type="hidden" name="estado" value="Activo">
                      <input type="hidden" name="property" value="0">
                      <input type="hidden" name="proyecto" value="{{$prop->id}}">
                    <button type="submit" class="form-control  col-md-6 col-xs-12 btn btn-success">Actualizar Estado</button>
                  </form>

                </div>
                @else
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <form  action="{{url('bloquear_property',$planActual->id)}}" method="post">
                    {{ csrf_field() }}
                    <input type="text"  disabled class="form-control col-md-6 col-xs-12" value="Desactivo">
                    <br>
                    <input type="hidden" name="estado" value="Desactivo">
                    <input type="hidden" name="property" value="0">
                    <input type="hidden" name="proyecto" value="{{$prop->id}}">
                    <button type="submit" class="form-control  col-md-6 col-xs-12 btn btn-danger">Actualizar Estado</button>
                  </form>
                </div>
                @endif
              </div>
              @endforeach
              <hr><p>Inmuebles</p>
              @foreach ($property as $prop)

              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Propiedad
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  disabled class="form-control col-md-7 col-xs-12" value="{{$prop->name}}">
                </div>

              </div>
              <div class="form-group col-md-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                </label>
                @if($prop->status == 1)
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <form action="{{url('bloquear_property',$planActual->id)}}" method="post">
                    {{ csrf_field() }}
                    <input type="text"  disabled class="form-control col-md-6 col-xs-12" value="Activo">
                    <br>
                    <input type="hidden" name="estado" value="Activo">
                      <input type="hidden" name="property" value="{{$prop->id}}">
                      <input type="hidden" name="proyecto" value="0">
                    <button type="submit" class="form-control  col-md-6 col-xs-12 btn btn-success">Actualizar Estado</button>
                  </form>

                </div>
                @else
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <form  action="{{url('bloquear_property',$planActual->id)}}" method="post">
                    {{ csrf_field() }}
                    <input type="text"  disabled class="form-control col-md-6 col-xs-12" value="Desactivo">
                    <br>
                    <input type="hidden" name="estado" value="Desactivo">
                    <input type="hidden" name="property" value="{{$prop->id}}">
                    <input type="hidden" name="proyecto" value="0">
                    <button type="submit" class="form-control  col-md-6 col-xs-12 btn btn-danger">Actualizar Estado</button>
                  </form>
                </div>
                @endif
              </div>

              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
@endsection
