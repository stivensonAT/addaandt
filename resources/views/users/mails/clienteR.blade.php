<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <!-- If you delete this meta tag, Half Life 3 will never be released. -->
  <meta name="viewport" content="width=device-width" />

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Bienvenida</title>

<style media="screen">
/* -------------------------------------
    GLOBAL
------------------------------------- */
* {
  margin:0;
  padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

img {
  max-width: 100%;
}
.collapse {
  margin:0;
  padding:0;
}
body {
  -webkit-font-smoothing:antialiased;
  -webkit-text-size-adjust:none;
  width: 100%!important;
  height: 100%;
}


/* -------------------------------------
    ELEMENTS
------------------------------------- */
a { color: #2BA6CB;}

.btn {
  text-decoration:none;
  color: #FFF;
  background-color: #666;
  padding:10px 16px;
  font-weight:bold;
  margin-right:10px;
  text-align:center;
  cursor:pointer;
  display: inline-block;
}

p.callout {
  padding:15px;
  background-color:#ECF8FF;
  margin-bottom: 15px;
}
.callout a {
  font-weight:bold;
  color: #2BA6CB;
}

table.social {
/* 	padding:15px; */
  background-color: #ebebeb;

}
.social .soc-btn {
  padding: 3px 7px;
  font-size:12px;
  margin-bottom:10px;
  text-decoration:none;
  color: #FFF;font-weight:bold;
  display:block;
  text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.tw { background-color: #1daced!important; }
a.gp { background-color: #DB4A39!important; }
a.ms { background-color: #000!important; }

.sidebar .soc-btn {
  display:block;
  width:100%;
}

/* -------------------------------------
    HEADER
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}


/* -------------------------------------
    BODY
------------------------------------- */
table.body-wrap { width: 100%;}


/* -------------------------------------
    FOOTER
------------------------------------- */
table.footer-wrap { width: 100%;	clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
  font-size:10px;
  font-weight: bold;

}


/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 37px;}
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

.collapse { margin:0!important;}

p, ul {
  margin-bottom: 10px;
  font-weight: normal;
  font-size:14px;
  line-height:1.6;
}
p.lead { font-size:17px; }
p.last { margin-bottom:0px;}

ul li {
  margin-left:5px;
  list-style-position: inside;
}

/* -------------------------------------
    SIDEBAR
------------------------------------- */
ul.sidebar {
  background:#ebebeb;
  display:block;
  list-style-type: none;
}
ul.sidebar li { display: block; margin:0;}
ul.sidebar li a {
  text-decoration:none;
  color: #666;
  padding:10px 16px;
/* 	font-weight:bold; */
  margin-right:10px;
/* 	text-align:center; */
  cursor:pointer;
  border-bottom: 1px solid #777777;
  border-top: 1px solid #FFFFFF;
  display:block;
  margin:0;
}
ul.sidebar li a.last { border-bottom-width:0px;}
ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



/* ---------------------------------------------------
    RESPONSIVENESS
    Nuke it from orbit. It's the only way to be sure.
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
  display:block!important;
  max-width:600px!important;
  margin:0 auto!important; /* makes it centered */
  clear:both!important;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
  padding:15px;
  max-width:600px;
  margin:0 auto;
  display:block;
}

/* Let's make sure tables in the content area are 100% wide */
.content table { width: 100%; }


/* Odds and ends */
.column {
  width: 300px;
  float:left;
}
.column tr td { padding: 15px; }
.column-wrap {
  padding:0!important;
  margin:0 auto;
  max-width:600px!important;
}
.column table { width:100%;}
.social .column {
  width: 280px;
  min-width: 279px;
  float:left;
}

/* Be sure to place a .clear element after each set of columns, just to be safe */
.clear { display: block; clear: both; }


/* -------------------------------------------
    PHONE
    For clients that support media queries.
    Nothing fancy.
-------------------------------------------- */
@media only screen and (max-width: 600px) {

  a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

  div[class="column"] { width: auto!important; float:none!important;}

  table.social div[class="column"] {
    width:auto!important;
  }

}
</style>
</head>

<body bgcolor="#FFFFFF">

  <!-- HEADER -->
  <table class="head-wrap" bgcolor="#3b5265">
    <tr>
      <td></td>
      <td class="header container" >

        <div class="content">
          <table bgcolor="#3b5265">
            <tr>
              <td><img src="http://embajador.quickinmobiliario.co/images/logo.png" /></td>

            </tr>
          </table>
        </div>

      </td>
      <td></td>
    </tr>
  </table><!-- /HEADER -->


  <!-- BODY -->
  <table class="body-wrap">
    <tr>
      <td></td>
      <td class="container" bgcolor="#FFFFFF">

        <div class="content">
          <table>
            <tr>
              <td>
                <h3>BIENVENIDO EMBAJADOR</h3>
                <p class="lead">En <strong>QUICK INN</strong> Buscamos personas como tu</p>
                <p>Apartir de este momento eres parte del equipo <strong>QUICK INN</strong> y podras aumentar tus ingresos de distintas formas, acontinuacion te explicamos como hacerlo</p>
                <br>

                <p><strong style="color: #ff7f00">1. QUICK TE OFRECE:</strong>Quick es una plataforma de inmuebles, con un amplio beneficio para las constructoras, debido a su facilidad de administracion, target y comisiones de venta, puesto que una constructora esta libre de poder vende sin tener que dar dinero a terceros, vende un plan de constructora y <strong>QUICK TE DA EL 5% POR CADA PLAN QUE VENDAS.</strong>Si necesitas material o informacion para poder vender contactanos, nosotros te asesoramos</p>
                <br>
                <p><strong style="color: #ff7f00">2. QUICK TE COMISIONA:</strong>Al publicar un inmueble en nuestra plataforma, se ofrece una comision de venta, vende el inmueble y ganate la comision,<strong> son mas de 60 millones, ofrecidos en comisiones.</strong> para mas informacion visita <a href="https://quickinmobiliario.com/">QUICK INN</a> busca inmuebles con comisiones altas, vendelos y <strong>GANA</strong> </p>
                <!-- social & contact -->
                </td>
              </tr>
              <tr>
                <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                  <br><br>Contáctanos: + + (57) 322 760 77 88 - 031 616 7994
                  <br>Bogotá - Colombia
                  <br>Skype: QuickINN
                  <br><br> Este mensaje y sus archivos adjuntos van dirigidos exclusivamente a su destinatario pudiendo contener información confidencial sometida a secreto profesional. No está permitida su reproducción o distribución sin la
                  autorización expresa de A.R.S. INVESTMENT COLOMBIA S EN C Si usted no es el destinatario final por favor elimínelo e infórmenos por esta vía. De acuerdo con la Ley Estatutaria 1581 de 2.012 de Protección de Datos y sus normas
                  reglamentarias, el Titular presta su consentimiento para que sus datos, facilitados voluntariamente, pasen a formar parte de una base de datos, cuyo responsable es A.R.S. INVESTMENT COLOMBIA S EN C, cuyas finalidades son: gestión
                  administrativa de la entidad, así como la gestión de carácter comercial o envío de comunicaciones comerciales sobre nuestros productos y/o servicios.
                  <br><br> Puede usted ejercer los derechos de acceso, corrección, supresión, revocación o reclamo por infracción sobre sus datos con un escrito dirigido a A.R.S. INVESTMENT COLOMBIA S EN C a la dirección de correo electrónico
                  arsinvestmentcol@gmail.com indicando en el asunto el derecho que desea ejercer; o mediante correo postal remitido a Cl 94 A NO.11A 27 OFC 201 Bogotá.
                  <br><br>
                </td>
              </tr>
            </table>
          </div><!-- /content -->


    </table><!-- /BODY -->

  </body>
  </html>
