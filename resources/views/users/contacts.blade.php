@extends('layouts.app')
@section('content')

<!-- page content -->
<div class="right_col" role="main">

    <!-- Table content -->
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Listado <small>Usuarios</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            Use el buscador para facilitar la consulta del usuario,  si da click encima del nombre o documento podrá editar y ver mas información del usuario, tambien puede descargar hacer unso de la información en formato PDF, EXCEL Y CSV
                        </p>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                                <tr>

                                    <th>Nombre de usuario</th>
                                    <th>email</th>
                                    <th>Asunto</th>
                                    <th>Origen</th>
                                    <th>Estado</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tableContacts as $userTable)
                                @if($userTable->status == 0)
                                <tr style=" background: #ccc; color: #000080">
                                    @else
                                <tr>
                                    @endif
                                    <td><a href="{{route('contacto_detail_phat', $userTable->id)}}">{{$userTable->name}}</a></td>
                                    <td><a href="{{route('contacto_detail_phat', $userTable->id)}}">{{$userTable->email}} </a></td>
                                    <td>{{$userTable->type}}</td>
                                    <td>{{$userTable->origin}}</td>

                                    <td><a href="{{route('contacto_detail_phat', $userTable->id)}}">
                                            @if($userTable->status == 0)
                                            <div class="text-center"> 
                                                <i style="color: #000080" title="No Leido" class="fa fa-envelope fa-2x" aria-hidden="true" ></i>
                                            </div> 
                                            @elseif($userTable->status == 1)
                                            <div class="text-center"> 
                                                <i title="Leido" class="fa fa-eye fa-2x" aria-hidden="true" ></i>
                                            </div> 
                                            @elseif($userTable->status == 2)
                                            <div class="text-center"> 
                                                <i title="Respondido" class="fa fa-paper-plane-o fa-2x" aria-hidden="true" ></i>
                                            </div> 
                                            @endif
                                        </a> </td>
                                    <td>{{$userTable->created_at}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
@endsection
