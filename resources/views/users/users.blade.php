@extends('layouts.app')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Listado <small>Usuarios</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              Use el buscador para facilitar la consulta del usuario,  si da click encima del nombre o documento podrá editar y ver mas información del usuario, tambien puede descargar hacer unso de la información en formato PDF, EXCEL Y CSV
            </p>
            <table id="datatable-buttons" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Usuario</th>
                  <th>Email</th>
                  <th>Celular/telefono</th>
                  <th>target</th>
                  <th>Registro</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tableUsers as $userTable)
                <tr>
                  <td><a href="{{route('detalles_usuario_phat', $userTable->id)}}">{{$userTable->first_name}}</a></td>
                  <td><a href="{{route('detalles_usuario_phat', $userTable->id)}}">{{$userTable->username}}
                    @if($userTable->profile_id != 2)
                    (Admi-{{$userTable->profile_id}})
                    @endif
                  </a></td>
                  <td><a href="{{route('detalles_usuario_phat', $userTable->id)}}">{{$userTable->email}}</a></td>
                  <td>{{$userTable->cell_phone}} / {{$userTable->phone}}</td>
                  <td><a href="{{route('detalles_usuario_phat', $userTable->id)}}">
                    @if($userTable->user_type_id==1)
                    Natural
                    @elseif($userTable->user_type_id==2)
                    Inmobiliaria
                      @elseif($userTable->user_type_id==3)
                      Constructora
                        @elseif($userTable->user_type_id==4)
                        Teso
                        @endif
                      </a></td>
                  <td>{{date_format($userTable->created_at,'d-M-y ')}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
@endsection
