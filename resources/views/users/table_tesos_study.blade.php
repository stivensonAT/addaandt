@extends('layouts.app')
@section('content')

<!-- page content -->
<div class="right_col" role="main">

  <!-- Table content -->
  <div class="">
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Listado <small>Usuarios</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <p class="text-muted font-13 m-b-30">
              Use el buscador para facilitar la consulta del usuario,  si da click encima del nombre o documento podrá editar y ver mas información del usuario, tambien puede descargar hacer unso de la información en formato PDF, EXCEL Y CSV
            </p>
            <table id="datatable-buttons" class="table table-striped table-bordered">
              <thead>
                <tr>

                  <th>Nombre de usuario</th>
                  <th>email</th>
                  <th>Identificacion</th>
                  <th>Direccion</th>
                  <th>Estado</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tableUsers as $userTable)
                @if($userTable->activo == 1 && $userTable->target == 4 )
                <tr>
                  <td><a href="{{route('detalles_usuario_email_phat', $userTable->email)}}">{{$userTable->username}}</a></td>
                  <td><a href="{{route('detalles_usuario_email_phat', $userTable->email)}}">{{$userTable->email}} </a></td>
                  <td>{{$userTable->document}}</td>

                  <td>{{$userTable->address}}</td>
                    <td>
                  @if($userTable->landing_page_seller == 0)
                  Proceso
                  @elseif($userTable->landing_page_seller == 2)
                  Desaprobado
                  @endif
                </td>

                </tr>
                @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
@endsection
