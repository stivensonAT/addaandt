
@extends('layouts.app')

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>{{$all_date->username}}</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Edita <small>Formulario de edicion</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <a href="{{route('usuario_teso_estudio_phat')}}">
              <button type="submit" class="btn btn-warning">Estudio Tesos</button>
            </a>
            </div>
          </div>
          <div class="x_content">

            <br />
            <form method="POST" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('editar_usuario',$all_date->id) }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">id en servidor<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"   class="form-control col-md-7 col-xs-12" value="{{$all_date->id}}" readonly="true">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="first_name" name="first_name" required="required" class="form-control col-md-7 col-xs-12" value="{{$all_date->first_name}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="username" name="username" required="required" class="form-control col-md-7 col-xs-12" value="{{$all_date->username}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="business_name">Nombre Empresarial
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="business_name" name="business_name"  class="form-control col-md-7 col-xs-12" value="{{$all_date->business_name}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cell_phone">Celular<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="cell_phone" name="cell_phone"  class="form-control col-md-7 col-xs-12" value="{{$all_date->cell_phone}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Telefono
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="phone" name="phone"  class="form-control col-md-7 col-xs-12" value="{{$all_date->phone}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Direccion
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="address" name="address"  class="form-control col-md-7 col-xs-12" value="{{$all_date->address}}">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo Documento<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="document_type" class="form-control">
                    <option value="{{$all_date->document_type}}">{{$all_date->document_type}}</option>
                    <option value="RUT">RUT</option>
                    <option value="NIT">NIT</option >
                      <option value="CC">Cedula Ciudadania</option >
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="document">Documento<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" id="document" name="document"  class="form-control col-md-7 col-xs-12" value="{{$all_date->document}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="age">Edad
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="number" id="age" name="age"  class="form-control col-md-7 col-xs-12" value="{{$all_date->age}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Genero</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      @if($all_date->gender =="Mujer")
                      <div id="gender" class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                          <input type="radio" name="gender" value="Hombre"> &nbsp; Hombre &nbsp;
                        </label>
                        <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                          <input type="radio" name="gender" value="Mujer"> Mujer
                        </label>
                      </div>
                      @else
                      <div id="gender" class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                          <input type="radio" name="gender" value="Mujer"> &nbsp; Mujer &nbsp;
                        </label>
                        <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                          <input type="radio" name="gender" value="Hombre"> Hombre
                        </label>
                      </div>
                      @endif
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">email<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="{{$all_date->email}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="profession">profesion
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" id="profession" name="profession"  class="form-control col-md-7 col-xs-12" value="{{$all_date->profession}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">city
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" id="city" name="city"  class="form-control col-md-7 col-xs-12" value="{{$all_date->city}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Target</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <select name="user_type_id" class="form-control">

                        <option value="{{$all_date->user_type_id}}">
                          @if($all_date->user_type_id== 1)
                          Natural
                          @elseif($all_date->user_type_id== 2)
                          Inmobiliaria
                          @elseif($all_date->user_type_id== 3)
                          Constructora
                          @elseif($all_date->user_type_id== 4)
                          Acelerador
                          @endif
                        </option>
                        <option value="1">Natural</option>
                        <option value="2">Inmobiliaria</option >
                          <option value="3">Constructora</option >
                            <option value="4">Acelerador</option >
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12"> <strong style="color: #ff7f00">Nivel Teso</strong> </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="landing_page_seller" class="form-control">

                              @if($all_date->landing_page_seller == 0)
                              <option value="{{$all_date->landing_page_seller}}">En Estudio</option>
                              <option value="1">Aprobado</option>
                              <option value="2">No Aprobado</option>
                              @elseif($all_date->landing_page_seller == 1)
                              <option value="{{$all_date->landing_page_seller}}">Aprobado</option>
                              <option value="0">En Estudio</option>
                              <option value="2">No Aprobado</option>
                              @elseif($all_date->landing_page_seller == 2)
                              <option value="{{$all_date->landing_page_seller}}">No Aprobado</option>
                              <option value="0">En Estudio</option>
                              <option value="1">Aprobado</option>
                              @endif

                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12"> <strong style="color: #ff7f00">Email Teso</strong></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="email_teso" class="form-control">
                                <option value="1">NO ENVIAR EMAIL</option>
                              <option value="0">SI ENVIAR EMAIL </option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12"> Level Bloqueado</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="bloqueado" class="form-control">
                              <option value="{{$all_date->bloqueado}}">
                                {{$all_date->bloqueado}}
                              </option>
                              <option value="0">No</option>
                              <option value="1">Si</option>
                              <option value="2">Incumplido</option>
                              <option value="3">Problemas Legales</option>
                              <option value="4">Llamar a la policia</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Level Perfil</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="profile_id" class="form-control">
                              <option value="{{$all_date->profile_id}}">                        {{$all_date->profile_id}}
                              </option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                            </select>
                          </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Actualizar</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /page content -->
          @endsection
