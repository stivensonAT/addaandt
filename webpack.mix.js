let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.copyDirectory('resources/assets/build', 'public/build');
mix.copyDirectory('resources/assets/css', 'public/css');
mix.copyDirectory('resources/assets/images', 'public/images');
mix.copyDirectory('resources/assets/js', 'public/js');
mix.copyDirectory('resources/assets/less', 'public/less');
mix.copyDirectory('resources/assets/sass', 'public/sass');
mix.copyDirectory('resources/assets/vendors', 'public/vendors');
